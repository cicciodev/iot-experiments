https://gitlab.com/cicciodev/iot-experiments

Installare ESP su Arduino IDE

1 - Aprire Arduino IDE
2 - Aprire Impostazione su File->Impostazione
3 - incollare i seguenti link nel campo "URL aggiuntive per il gestore schede" e premere OK:
    
  https://raw.githubusercontent.com/Lauszus/Sanguino/master/package_lauszus_sanguino_index.json
  https://arduino.esp8266.com/stable/package_esp8266com_index.json
  https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json

4 - Aprire Gestore schede su "Strumenti"->"Schede"->"Gestore schede"
5 - Nella ricerca scriviamo "esp32", dovrebbe esserci un solo risultato e basta premere "Installa"

